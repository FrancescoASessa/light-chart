<?php
#Autoloading
require __DIR__ . '/../vendor/autoload.php';

use FrancescoASessa\LightChart\Chart;
use FrancescoASessa\LightChart\Column;

$chart = new Chart([
	'title' => 'Grafico di prova',
	'font' => '',	//Font dei titoli
	'color' => '',	//Colore delle colonne
]);

$columns = [
	new Column('PHP', 80),
	new Column('Javascript', 44),
	new Column('Javascript', 234),
	new Column('Javascript', 456),
	new Column('Javascript', 100),
	new Column('Javascript', 270),
	new Column('Javascript', 370),
	new Column('Javascript', 170),
	new Column('Javascript', 570),
	new Column('Javascript', 70)
];


$chart->setColumns($columns);

echo $chart->printStyle();
echo $chart->toHtml();
