<?php

namespace FrancescoASessa\LightChart;

class Column
{
	public $title;

	public $value;

	function __construct( string $title, int $value)
	{
		$this->title = $title;
		$this->value = $value;
	}
}