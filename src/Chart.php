<?php

namespace FrancescoASessa\LightChart;

class Chart
{
	protected $globalOption;

	protected $columns;

	public $font; //TODO
	public $color; //TODO
	public $title; //TODO
	public $step = 5;

	function __construct(array $options = ['color' => '#666' , 'font' => 'Lucida Grande'])
	{
		if(isset($options['font'])) $this->setGlobalOption('font', $options['font']);
		if(isset($options['color'])) $this->setGlobalOption('color',$options['color']);

		if(isset($options['title']))$this->setGlobalOption('title', $options['title']);
	}

	public function setGlobalOption($key, $value)
	{
		$this->$key = $value;
	}

	public function setColumn(Column $column)
	{
		array_push($this->columns, $column);
		return $this;
	}

	public function setColumns($columns)
	{
		$columns = array_filter($columns,function($column){
			return ($column instanceof Column);
		});

		if(count($columns) > 0){
			$this->columns = $columns;
			return $this;
		}else{
			throw new Exception('Array di colonne non valido', 1);
		}
	}

	public function getColumns()
	{
		return $this->columns;
	}

	public function toHtml()
	{
		return (new HtmlChart($this))->draw();
	}
}