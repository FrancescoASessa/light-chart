<?php

namespace FrancescoASessa\LightChart;

class HtmlChart
{
	protected $chart;

	private $html;

	private $array;

	function __construct(Chart $chart)
	{
		$this->chart = $chart;
		$this->array = $this->toArray();
	}

	private function setHtml($html)
	{
		$this->html = $this->html . $html;
	}

	public function toArray()
	{
		$html = [];

		$step = $this->chart->step;
		$columns = $this->chart->getColumns();

		$max = max(array_column($columns, 'value'));

		array_push($html, "<div class='chart'>");

		for ($i=1; $i <= count($columns); $i++) { 
			$title = $columns[$i -1]->title;
			$value = $columns[$i -1]->value;

			$perc = round(($value/$max) * 100);

			array_push($html, "<div class='bar-$perc'></div>");
		}

		array_push($html, "</div>");

		return $html;
	}


	private static function loadStyle()
	{
		return file_get_contents(__DIR__ . '\style.css');
	}

	public static function printStyle()
	{
		$style = HtmlChart::loadStyle();
		return "<style>$style</style>";
	}

	public function draw()
	{
		$this->setHtml(implode('',$this->array));
		return $this->html;
	}
}